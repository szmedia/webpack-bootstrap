# Webpack bootstrap package

Clone this repository for a clean webpack workflow startingpoint.

## Features
- TypeScript
- Scss
- Bootstrap
- Dev Server

## Install
`npm install`

## Building

### Dev server:

`npm run start`

### Production build:

`npm run build`

## ToDo's
- stylelint
- Improve/add typescript conditions?